//
//  FullMenuViewController.swift
//  WAEmergencies
//
//  Created by Nicholas Petersilge on 12/27/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVAudioPlayerDelegate {
    
    var player:AVAudioPlayer = AVAudioPlayer()
    
    @IBAction func Play(_sender: Any) {
        player.play()
    }
    @IBAction func play(_ sender: Any) {
        
    }
   
    
    override func viewDidLoad() {
        
        
        do {
            let audioPlayer = Bundle.main.path (forResource: "abdominal thrusts", ofType: "mp3")
            
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPlayer!) as URL)
        }
        catch {
            //ERROR
            
        }
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
